module.exports = {
    entry: [
        './src/components/HamletServicesApp.js'
    ],
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',
            query: {
                presets: ['react', 'es2015'],
                plugins: ["transform-object-rest-spread"]
            }
        },
            {test: /\.json$/, loader: 'json'}
        ]
    }
};