import {connect} from 'react-redux';
import Login from '../components/login/Login.js';
import {generateOtpBtnClicked, mobileNoTxtChanged, otpTxtChanged, login} from '../actions/login/LoginActions.js';

const mapStateToProps = (state, ownProps) => {
    return {
        otpGenerationStatus: state.login.otp_generation_status,
        otpError: state.login.otp_error,
        loginError: state.login.login_error,
        loginStatus: state.login.login_state
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        generateOtpBtnClicked: () => {
            dispatch(generateOtpBtnClicked());
        },
        mobileNoTxtChanged: (mobileNo) => {
            dispatch(mobileNoTxtChanged(mobileNo));
        },
        onLoginBtnClicked: () => {
            dispatch(login());
        },
        otpTxtChanged: (otp) => {
            dispatch(otpTxtChanged(otp));
        }
    }
}

const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);

export default LoginContainer;