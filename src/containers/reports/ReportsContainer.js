import {connect} from 'react-redux';
import Reports from '../../components/reports/Reports.js';

const mapStateToProps = (state, ownProps) => {
    return {
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

const ReportsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Reports);

export default ReportsContainer;