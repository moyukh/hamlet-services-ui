import {connect} from 'react-redux';
import Campaign from '../../components/campaign/Campaign.js';

const mapStateToProps = (state, ownProps) => {
    return {
        selectedNeighbourhoods: state.campaign.selected_neighbourhoods
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

const CampaignContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Campaign);

export default CampaignContainer;