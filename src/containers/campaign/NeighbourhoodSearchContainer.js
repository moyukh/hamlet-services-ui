import {connect} from 'react-redux';
import SearchBox from '../../components/campaign/SearchBox.js';
import {neighbourhoodSearchTxtChange} from '../../actions/campaign/CampaignActions.js';

const mapStateToProps = (state, ownProps) => {
    return {
        searchTxt: state.campaign.search_text
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        searchTxtChange: (neighbourhoodName) => {
            dispatch(neighbourhoodSearchTxtChange(neighbourhoodName));
        }
    }
}

const NeighbourhoodSearchContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchBox);

export default NeighbourhoodSearchContainer;