import {connect} from 'react-redux';
import PostCard from '../../components/campaign/PostCard.js';
import {removeNeighbourhood, postImgUpload, postTxtChange, sendPost} from '../../actions/campaign/CampaignActions.js';

const mapStateToProps = (state, ownProps) => {
    return {
        selectedNeighbourhoods: state.campaign.selected_neighbourhoods,
        imgFile: state.campaign.post.image.file,
        progressPercent: state.campaign.post.image.upload_percent,
        postTxt: state.campaign.post.text,
        imgUrl: state.campaign.post.image.url,
        postSentStatus: state.campaign.post.status
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onRemoveNeighbourhood: (neighbourhood_id) => {
            dispatch(removeNeighbourhood(neighbourhood_id));
        },
        postImgUpload: (file) => {
            dispatch(postImgUpload(file));
        },
        postTxtChange: (post_txt) => {
            dispatch(postTxtChange(post_txt));
        },
        postSendClick: () => {
            dispatch(sendPost());
        }
    }
}

const PostCardContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(PostCard);

export default PostCardContainer;