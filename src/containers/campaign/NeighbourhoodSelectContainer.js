import {connect} from 'react-redux';
import NeighbourhoodSelect from '../../components/campaign/NeighbourhoodSelect.js';
import {neighbourhoodNameClick} from '../../actions/campaign/CampaignActions.js';

const mapStateToProps = (state, ownProps) => {
    return {
        subscribedNeighbourhoods: state.neighbourhoods.subscribed,
        searchTxt: state.campaign.search_text,
        selectedNeighbourhoodsMap: state.campaign.selected_neighbourhoods_map
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onNeighbourhoodOptionClicked: (neighbourhood_id, neighbourhood_name) => {
            dispatch(neighbourhoodNameClick({neighbourhood_id, neighbourhood_name}));
        }
    }
}

const NeighbourhoodSelectContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(NeighbourhoodSelect);

export default NeighbourhoodSelectContainer;