import {connect} from 'react-redux';
import Registration from '../components/registration/Registration.js';
import {loginOtpClicked} from '../actions/login/LoginActions.js';

const mapStateToProps = (state, ownProps) => {
    return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

const RegistrationContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Registration);

export default RegistrationContainer;
