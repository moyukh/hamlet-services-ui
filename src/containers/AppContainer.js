import {connect} from 'react-redux';
import AppHolder from '../components/AppHolder.js';
import {logout} from '../actions/login/LoginActions.js';

const mapStateToProps = (state, ownProps) => {
    return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        logoutClick: () => {
            dispatch(logout());
        }
    }
}

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AppHolder);

export default AppContainer;