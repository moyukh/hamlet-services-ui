import * as Action from '../ActionConstants.js';
import HamletBackend from '../../commons/backend/HamletBackend.js';

let hamletBackend = new HamletBackend();

export const neighbourhoodSearchTxtChange = (neighbourhood_name) => {
    return {
        type: Action.NEIGHBOURHOOD_NAME_TXT_CHANGE,
        neighbourhood_name
    };
}

export const neighbourhoodNameClick = (neighbourhood) => {
    return {
        type: Action.CAMPAIGN_NEIGHBOURHOOD_NAME_CLICK,
        ...neighbourhood
    }
}

export const removeNeighbourhood = (neighbourhood_id) => {
    return {
        type: Action.CAMPAIGN_NEIGHBOURHOOD_NAME_REMOVE,
        neighbourhood_id
    }
}

export const postImgUpload = (file) => {
    return (dispatch, getState) => {
        dispatch(postImgUploadPending(file));
        hamletBackend.uploadImage(file, getState(), function (progressPercent) {
                dispatch(postImgUploadProgress(progressPercent));
            },
            function (image_url) {
                dispatch(postImgUploadFinish(image_url));
            });
    }
}

const postImgUploadFinish = (image_url) => {
    return {
        type: Action.POST_IMG_UPLOAD_SUCCESS,
        image_url
    }
}

const postImgUploadPending = (file) => {
    return {
        type: Action.POST_IMG_UPLOAD_PENDING,
        file
    };
}

const postImgUploadProgress = (progress_percent) => {
    return {
        type: Action.POST_IMG_UPLOAD_PROGRESS,
        progress_percent
    }
}

export const postTxtChange = (post_txt) => {
    return {
        type: Action.POST_TXT_CHANGE,
        post_txt
    }
}

export const sendPost = () => {
    return (dispatch, getState) => {
        dispatch(postSendClick());
        let post = getState().campaign.post.text;
        let neighbourhood_ids = getState().campaign.selected_neighbourhoods;
        let imageUrl = getState().campaign.post.image.url;

        hamletBackend.sendPost(post, neighbourhood_ids, imageUrl)
            .then(resp => resp.json())
            .then(json => {
                if (json.ok) {
                    dispatch(postSuccess());
                } else {
                    dispatch(postError(json.error.reason));
                }
            })
            .catch(err => {
                dispatch(postError(err.message));
            });

    }
}

export const createNewPost = () => {
    return {
        type: Action.CREATE_NEW_POST
    }
}

const postSuccess = () => {
    return {
        type: Action.POST_SEND_SUCCESS
    }
}

const postError = (err_message) => {
    return {
        type: Action.POST_SEND_FAILURE,
        err_message
    }
}

const postSendClick = () => {
    return {
        type: Action.POST_SEND_CLICK
    }
}