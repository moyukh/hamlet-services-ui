import * as Action from '../ActionConstants.js';
import HamletBackend from '../../commons/backend/HamletBackend.js';
import {isValidMobileNo} from '../../commons/utils/ValidationUtils.js';

let hamletBackend = new HamletBackend();

export const loginOtpClicked = () => {
    return {
        type: Action.GENERATE_LOGIN_OTP_CLICKED
    };
}

export const generateOtpBtnClicked = () => {
    return (dispatch, getState) => {
        let mobileNo = getState().login.mobile_no;

        if (!mobileNo || mobileNo.length < 10 || !isValidMobileNo(mobileNo)) {
            return dispatch(generateLoginOtpError('Invalid Mobile No.'));
        }

        dispatch(loginOtpClicked());
        hamletBackend.generateLoginOtp(mobileNo)
            .then(resp => resp.json())
            .then(json => {
                console.log();
                if (json.ok) {
                    dispatch(generateLoginOtpSuccess());
                } else {
                    dispatch(generateLoginOtpError(json.error.reason));
                }
            })
            .catch(err => {
                dispatch(generateLoginOtpError(err.message));
            });
    }
}

export const login = () => {
    return (dispatch, getState) => {
        let mobileNo = getState().login.mobile_no;
        let otp = getState().login.otp;

        if (!mobileNo || mobileNo.length < 10) {
            return dispatch(loginError('Invalid Mobile No.'));
        }

        if (!otp || otp.length < 4) {
            return dispatch(loginError('Invalid OTP'));
        }

        dispatch(loginClicked());
        hamletBackend.login(mobileNo, otp)
            .then(resp => resp.json())
            .then(json => {
                if (json.ok) {
                    dispatch(loginSuccess(json.response));
                } else {
                    dispatch(loginError(json.error.reason));
                }
            })
            .catch(err => {
                dispatch(loginError(err.message));
            });
    }
}

export const loginClicked = () => {
    return {
        type: Action.LOGIN_BUTTON_CLICK
    }
}

export const loginSuccess = (data) => {
    return {
        type: Action.LOGIN_SUCCESS,
        ...data
    }
}

export const loginError = (errMsg) => {
    return {
        type: Action.LOGIN_FAILURE,
        err_message: errMsg
    }
}

export const mobileNoTxtChanged = (mobile_no) => {
    return {
        type: Action.MOBILE_NO_TXT_CHANGE,
        mobile_no
    }
}

export const otpTxtChanged = (otp) => {
    return {
        type: Action.OTP_TXT_CHANGE,
        otp
    }
}

export const logout = () => {
    return {
        type: Action.LOGOUT
    }
}

const generateLoginOtpError = (errMsg) => {
    return {
        type: Action.GENERATE_LOGIN_OTP_ERROR,
        err_message: errMsg
    }
}

const generateLoginOtpSuccess = () => {
    return {
        type: Action.GENERATE_LOGIN_OTP_SUCCESS
    }
}