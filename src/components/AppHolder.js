import React, { PropTypes } from 'react';
import Menu from './menu/Menu.js';
import { RouteTransition } from 'react-router-transition';

const AppHolder = (props, context) => {
    return (
        <div className="app-holder">
            <Menu logoutClick={props.logoutClick}/>
            <main className="main-area">
                        {props.children}
            </main>
        </div>
    );
}

export default AppHolder;