import React from 'react';
import {render} from 'react-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger'
import {Router, Route, IndexRoute, browserHistory} from 'react-router';
import RootReducer from '../reducers/RootReducer.js';

import LoginContainer from '../containers/LoginContainer.js';
import Registration from './registration/Registration.js';
import ResetPassword from './resetpassword/ResetPassword.js';
import AppContainer from '../containers/AppContainer.js';
import CampaignContainer from '../containers/campaign/CampaignContainer.js';
import RegistrationContainer from '../containers/RegistrationContainer.js';
import Reports from './reports/Reports.js';
import MyProfile from './myprofile/MyProfile.js';

import {persistStore, autoRehydrate} from 'redux-persist';

const loggerMiddleware = createLogger();
//let store = createStore(RootReducer, compose(applyMiddleware(thunkMiddleware, loggerMiddleware)));
let store = createStore(RootReducer, compose(applyMiddleware(thunkMiddleware, loggerMiddleware), autoRehydrate()));
persistStore(store);

render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/">
                <IndexRoute component={LoginContainer}/>
                <Route path="register" component={Registration}/>
                <Route component={AppContainer}>
                    <Route path="campaign" component={CampaignContainer}/>
                    <Route path="reports" component={Reports}/>
                    <Route path="myprofile" component={MyProfile}/>
                </Route>
            </Route>
        </Router>
    </Provider>, document.getElementById('root')
);