import React, { PropTypes } from 'react';
import {Link} from 'react-router';

const Menu = (props, context) => {
    let {logoutClick} = props;
    let logoStyle = {width: "26px", height: "26px"};

    const logoutClicked = (e) => {
        logoutClick();
        context.router.push('/');
    }

    return (
        <header className="app_header">
            <div className="brand_logo">
                <div className="logo_first">
                    <img src="../../../assets/images/hamlet_logo.png" style={logoStyle}/>
                    <span className="myhamlet">MyHamlet</span>
                </div>
                <span className="services">Services</span>
            </div>
            <nav id="advanced-nav" className="advanced-nav menu" role="navigation">
                <ul>
                    <li>
                        <Link to="campaign" activeClassName="active">
                            <div className="icon">
                                <i className="fa fa-bullhorn"></i>
                            </div>
                            <div className="button-text">
                                Campaign
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to="reports" activeClassName="active">
                            <div className="icon">
                                <i className="fa fa-bar-chart"></i>
                            </div>
                            <div className="button-text">
                                Reports
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to="myprofile" activeClassName="active">
                            <div className="icon">
                                <i className="fa fa-user"></i>
                            </div>
                            <div className="button-text">
                                My Profile
                            </div>
                        </Link>
                    </li>
                </ul>
            </nav>
            <div className="logout">
                <i className="fa fa-sign-out" onClick={logoutClicked}></i>
            </div>
        </header>
    );
}

Menu.contextTypes = {
    router: React.PropTypes.object.isRequired
}

export default Menu;