import React, { PropTypes } from 'react';
import NeighbourhoodBubble from './NeighbourhoodBubble.js';
import FlipMove from 'react-flip-move';
import Dropzone from 'react-dropzone';

import SendButton from './SendButton.js';
import SentConfirmation from './SentConfirmation.js';

const PostCard = ({selectedNeighbourhoods, onRemoveNeighbourhood, postImgUpload, imgFile, progressPercent, postTxtChange, postTxt, imgUrl, postSentStatus, postSendClick}) => {

    const onDrop = (files) => {
        console.log('Received files: ', files);
        let file = files[0];

        postImgUpload(file);
    }

    const onPostTxtChange = (e) => {
        postTxtChange(e.target.value);
    }

    let postBtnStyle = {opacity: 0};

    if (postTxt || imgUrl) {
        postBtnStyle = {opacity: 1};
    }

    let dropZoneContent = '';
    let dropZoneStyles = 'drop_zone';

    let progressLineStyle = {width: '0%', visibility: 'hidden'};

    if (progressPercent && progressPercent !== 100) {
        progressLineStyle = {width: progressPercent + '%', visibility: 'visible'}
    }

    if (imgFile && imgFile.name) {
        dropZoneContent = (<img className="img_preview" src={imgFile.preview}/>);
    } else {
        dropZoneStyles += ' empty';
        dropZoneContent = (<div>Drop an image here, or click to upload image.</div>);
    }


    let neighbourhoodBubbles = selectedNeighbourhoods.map((neighbourhood) => {
        return (
            <NeighbourhoodBubble key={neighbourhood._id} neighbourhood_id={neighbourhood._id}
                                 name={neighbourhood.name} onRemoveNeighbourhood={onRemoveNeighbourhood}/>
        );
    });

    let buttonObj = null;
    if (postSentStatus) {
        buttonObj = (<SentConfirmation postSentStatus={postSentStatus}/>);
    } else {
        buttonObj = (<SendButton postBtnStyle={postBtnStyle} postSendClick={postSendClick}/>);
    }
    return (
        <section className="post_card">
            <FlipMove className="neighbourhood_bubbles">
                {neighbourhoodBubbles}
            </FlipMove>

            <Dropzone className={dropZoneStyles} onDrop={onDrop} multiple={false}>
                <div className="progress_line" style={progressLineStyle}></div>
                {dropZoneContent}

            </Dropzone>

            <textarea placeholder="Enter Post Content" rows="3" spellCheck={false}
                      onChange={onPostTxtChange}>{postTxt}</textarea>

            {buttonObj}
        </section>
    );
}

export default PostCard;