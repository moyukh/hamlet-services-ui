import React, { PropTypes } from 'react';
import {getFormattedSuggestion} from '../../commons/utils/StringUtils.js';
import FlipMove from 'react-flip-move';

import NeighbourhoodSelectOption from './NeighbourhoodSelectOption.js';

const NeighbourhoodSelect = ({subscribedNeighbourhoods, searchTxt, onNeighbourhoodOptionClicked, selectedNeighbourhoodsMap}) => {

    let neighbourhoodList = [];

    subscribedNeighbourhoods = subscribedNeighbourhoods.filter((neighbourhood) =>
        (!searchTxt || neighbourhood.name.toLowerCase().indexOf(searchTxt.toLowerCase()) > -1) && !selectedNeighbourhoodsMap.hasOwnProperty(neighbourhood._id)
    );

    neighbourhoodList = subscribedNeighbourhoods.map((neighbourhood) => {
        let {preMatch, match, postMatch} = getFormattedSuggestion(neighbourhood.name, searchTxt);
        return (
            <NeighbourhoodSelectOption key={neighbourhood._id} neighbourhood_id={neighbourhood._id}
                                       neighbourhood_name={neighbourhood.name}
                                       onNeighbourhoodOptionClicked={onNeighbourhoodOptionClicked}>
                {preMatch}<span>{match}</span>{postMatch}
            </NeighbourhoodSelectOption>
        );
    });

    return (
        <aside className="neighbourhood_select">
            <div className="select_neighbourhood_header">
                <div className="label">Select Neighbourhood</div>
                <div className="line"></div>
            </div>
            <div className="neighbourhood_list_container">
                <FlipMove typeName="ul" className="neighbourhood_list">
                    {neighbourhoodList}
                </FlipMove>
            </div>
        </aside>
    );
}

export default NeighbourhoodSelect;