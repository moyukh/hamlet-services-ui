import React, { Component, PropTypes } from 'react';

/*
 const NeighbourhoodSelectOption = ({neighbourhood_id, neighbourhood_name, children, onNeighbourhoodOptionClicked}) => {

 const neighbourhoodOptionClick = () => {
 onNeighbourhoodOptionClicked(neighbourhood_id, neighbourhood_name);
 }

 return (
 <li onClick={neighbourhoodOptionClick}>
 {children}
 </li>
 );
 }
 */

class NeighbourhoodSelectOption extends Component {
    constructor() {
        super();
        this.neighbourhoodOptionClick = this.neighbourhoodOptionClick.bind(this);
    }

    neighbourhoodOptionClick() {
        this.props.onNeighbourhoodOptionClicked(this.props.neighbourhood_id, this.props.neighbourhood_name);
    }

    render() {
        return (
            <li onClick={this.neighbourhoodOptionClick}>
                {this.props.children}
            </li>
        );
    }
}

export default NeighbourhoodSelectOption;