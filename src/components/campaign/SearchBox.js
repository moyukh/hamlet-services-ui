import React, { PropTypes } from 'react';

const SearchBox = ({searchTxt, searchTxtChange}) => {
    const onSearchTxtChange = (e) => {
        searchTxtChange(e.target.value);
    }
    return (
        <section className="search">
            <input spellCheck="false" placeholder="Enter Neighbourhood Name" autoFocus value={searchTxt}
                   onChange={onSearchTxtChange}/>
        </section>
    );
}

export default SearchBox;