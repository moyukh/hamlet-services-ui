import React, { PropTypes } from 'react';

const SendButton = ({postBtnStyle, postSendClick}) => {
    return (
        <div className="button raised clickable" style={postBtnStyle} onClick={postSendClick}>
            <input type="checkbox" className="toggle"/>

            <div className="anim"></div>
            <span><i className="fa fa-paper-plane" aria-hidden="true"></i></span>
            <span className="send_txt">Send</span>
        </div>
    );
}

export default SendButton;