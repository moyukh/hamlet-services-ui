import React, { Component, PropTypes } from 'react';
/*
 const NeighbourhoodBubble = ({name, neighbourhood_id, onRemoveNeighbourhood}) => {

 const removeNeighbourhoodClick = () => {
 onRemoveNeighbourhood(neighbourhood_id);
 }

 return (
 <span className="neighbourhood_bubble">
 <span className="text">{name}</span>
 <span className="cancel_btn" onClick={removeNeighbourhoodClick}>x</span>
 </span>
 );
 }
 */

class NeighbourhoodBubble extends Component {
    constructor() {
        super();
        this.handleRemoveClick = this.handleRemoveClick.bind(this);
    }

    handleRemoveClick() {
        this.props.onRemoveNeighbourhood(this.props.neighbourhood_id);
    }

    render() {
        return (
            <span className="neighbourhood_bubble">
                <span className="text">{this.props.name}</span>
                <span className="cancel_btn" onClick={this.handleRemoveClick}>x</span>
            </span>
        );
    }
}

export default NeighbourhoodBubble;