import React, { PropTypes } from 'react';
//import SearchBox from './SearchBox.js';
import NeighbourhoodSelectContainer from '../../containers/campaign/NeighbourhoodSelectContainer.js';
import PostCard from './PostCard.js';
import NeighbourhoodSearchContainer from '../../containers/campaign/NeighbourhoodSearchContainer.js';
import PostCardContainer from '../../containers/campaign/PostCardContainer.js';

const Campaign = () => {
    return (
        <div className="campaign-module-content">
            <NeighbourhoodSearchContainer />
            <article>
                <NeighbourhoodSelectContainer />

                <PostCardContainer />
            </article>
        </div>
    );
}

export default Campaign;