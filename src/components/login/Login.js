import React, { PropTypes, Component } from 'react';
import {Link} from 'react-router';
import {deleteNonNumericChar} from '../../commons/utils/InputUtils.js';

class Login extends Component {

    componentWillUpdate(nextProps, nextState, nextContext) {
        console.log(nextProps);
        let {loginStatus}=nextProps;
        console.log(loginStatus);
        if (loginStatus === 'success')
            nextContext.router.push('/campaign');
    }

    render() {
        let {generateOtpBtnClicked, otpGenerationStatus, mobileNoTxtChanged, otpError, loginError, loginStatus, onLoginBtnClicked, otpTxtChanged} = this.props;
        let mobileNoNodeRef = null;

        let otpButtonDisabled = otpGenerationStatus && otpGenerationStatus !== 'err';

        let frontClassNames = 'front';
        frontClassNames += (otpGenerationStatus === 'success') ? ' flipped' : '';
        let backClassNames = 'back';
        backClassNames += (otpGenerationStatus === 'success') ? ' flipped' : '';

        let otpBtnTxt = 'Send OTP';
        if (otpGenerationStatus === 'clicked')
            otpBtnTxt = 'Generating OTP...';

        const setMobileNoRef = (node) => {
            mobileNoNodeRef = node;
        }

        const otpBtnClick = (e) => {
            e.preventDefault();
            generateOtpBtnClicked();
        }

        const onMobileNoTxtChange = (e) => {
            deleteNonNumericChar(e);
            mobileNoTxtChanged(e.target.value);
        }

        const onOTPTxtChange = (e) => {
            deleteNonNumericChar(e);
            otpTxtChanged(e.target.value);
        }

        const loginClicked = (e) => {
            e.preventDefault();
            onLoginBtnClicked();
        }

        return (
            <div className="landing_page">
                <div className="brand_logo">
                    <div className="logo_first">
                        <img src="../../../assets/images/hamlet_logo.png"/>
                        <span className="myhamlet">MyHamlet</span>
                    </div>
                    <span className="services">Services</span>
                </div>
                <main className="landing_main">
                    <section className="middle_section">
                        <div className="description">
                            <p className="moto">Reach out to residents of different neighbourhoods.</p>

                            <p className="keywords">Communicate. Interact. Engage.</p>

                            <div className="download_icons">
                                <a href=""><img src="../../../assets/images/play_store_badge.png"/></a>
                                <a href=""><img src="../../../assets/images/ios-app-store-badge.png"/></a>
                            </div>
                        </div>
                        <div className="login_card">
                            <div className={frontClassNames}>
                                <form>
                                    <div className="caption">Sign In</div>
                                    <input type="text" name="mobile_no" placeholder="Mobile No." maxLength="10"
                                           ref={setMobileNoRef} onChange={onMobileNoTxtChange}/>

                                    <div className="error">{otpError}</div>

                                    <input type="submit" value={otpBtnTxt} onClick={otpBtnClick}
                                           disabled={otpButtonDisabled}/>

                                    <div className="signup">
                                        <span className="new_service_question">New Service?</span><Link to="/register">Sign
                                        Up</Link>
                                    </div>
                                </form>
                            </div>
                            <div className={backClassNames}>
                                <form>
                                    <input type="text" name="otp" placeholder="Enter OTP" maxLength="4"
                                           onChange={onOTPTxtChange}/>

                                    <div className="error">{loginError}</div>

                                    <input type="submit" value="LOGIN" disabled={loginStatus === 'clicked'}
                                           onClick={loginClicked}/>
                                </form>
                            </div>
                        </div>
                    </section>
                </main>
            </div>
        );
    }
}

Login.contextTypes = {
    router: React.PropTypes.object.isRequired
}

export default Login;