export const isValidMobileNo = (mobileNo) => {
    let indianNoPattern = /^[789]\d{9}$/;
    return indianNoPattern.test(mobileNo);
}