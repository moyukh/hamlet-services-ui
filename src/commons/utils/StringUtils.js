export const getFormattedSuggestion = (suggestion, searchTxt) => {
    let startIndex = suggestion.toLowerCase().indexOf(searchTxt.toLowerCase());
    let endIndex = startIndex + searchTxt.length;

    let preMatch = suggestion.substring(0, startIndex);
    let match = suggestion.substring(startIndex, endIndex);
    let postMatch = suggestion.substring(endIndex);

    return {preMatch, match, postMatch}
}