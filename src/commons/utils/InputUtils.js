export const deleteNonNumericChar = (e) => {
    let endChar = e.target.value.substring(e.target.value.length - 1);
    if (isNaN(endChar)) {
        e.target.value = e.target.value.substring(0, e.target.value.length - 1);
    }
}