export const GENERATE_LOGIN_OTP = '/login/otp';
export const LOGIN = '/login';
export const GET_NEIGHBOURHOODS_FOR_CITY = '/neighbourhoods';
export const UPLOAD_IMAGE = '/upload/image';

export const SEND_POST = '/post';