import BaseBackend from './BaseBackend.js';
import ApiConfig from '../../../staticdata/apiconfig.json';
import * as HttpMethod from './HttpMethodConstants.js';
import * as ApiEndpoints from './ApiEndpoints.js';

export default class HamletBackend extends BaseBackend {
    constructor() {
        super(ApiConfig['base-url'], ApiConfig['api-key']);
    }

    generateLoginOtp(mobile_no) {
        return this._fetch(HttpMethod.POST, ApiEndpoints.GENERATE_LOGIN_OTP, {mobile_no});
    }

    login(mobile_no, otp) {
        return this._fetch(HttpMethod.POST, ApiEndpoints.LOGIN, {mobile_no, otp})
            .then(resp => {
                return Promise.resolve(resp);
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }

    uploadImage(file, state, progressCallback, doneCallback) {
        this._upload(HttpMethod.POST, ApiEndpoints.UPLOAD_IMAGE, file, state, progressCallback, doneCallback);
    }

    sendPost(post, neighbourhood_ids, image_url) {
        return this._fetch(HttpMethod.POST, ApiEndpoints.SEND_POST, {post, neighbourhood_ids, image_url});
    }
}