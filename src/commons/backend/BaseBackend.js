import 'whatwg-fetch';

export default class BaseBackend {
    constructor(apiBaseUrl, apiKey) {
        this.API_BASE_URL = apiBaseUrl;
        this.API_KEY = apiKey;
    }

    _fetch(httpMethod, endpoint, data, state) {

        let opts = {
            method: 'GET',
            mode: 'cors',
            cache: 'no-store'
            //credentials: 'include'
        };
        if (httpMethod)
            opts.method = httpMethod;

        console.log(this.API_BASE_URL + endpoint);
        opts.url = this.API_BASE_URL + endpoint;
        opts.headers = {};
        opts.headers['x-hamlet-api-key'] = this.API_KEY;

        if (state && state.session && state.session.sessiontoken)
            opts.headers['x-hamlet-sessiontoken'] = state.session.sessiontoken;

        opts.headers['event-ts'] = new Date().getTime();
        opts.headers['event-id'] = 'hamletsp_dashboard_' + new Date().getTime();


        if (data) {
            opts.body = JSON.stringify(data);
        }

        return fetch(opts.url, opts);
    }

    _upload(httpMethod, endpoint, file, state, progressCallback, doneCallback) {
        /*
         var xhr = new XMLHttpRequest();
         xhr.open(httpMethod, 'http://hamletqa.zip.pr/hamlet/main/image', true);
         xhr.setRequestHeader("Content-type", "multipart/form-data");

         xhr.onload = handler;

         function handler() {
         if (this.status == 200) {
         console.log('done');
         doneCallback();
         } else {
         console.log('some error occurred');
         }
         }


         xhr.upload.onprogress = function (e) {
         let percentProgress = 0;
         if (e.lengthComputable) {
         percentProgress = (e.loaded / e.total) * 100;
         console.log(percentProgress);
         if (progressCallback) {
         progressCallback(percentProgress);
         }
         }
         };

         console.log('sending file');
         console.log(file);
         xhr.send(file);
         */

        let data = new FormData();
        data.append("image", file);

        let xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                console.log(this.responseText);
                let json = JSON.parse(this.responseText);
                if (doneCallback) {
                    doneCallback(json.response.image_url);
                }
            }
        });

        xhr.upload.onprogress = function (e) {
            console.log(e);
            let percentProgress = 0;
            if (e.lengthComputable) {
                percentProgress = e.loaded / e.total * 100;
                if (progressCallback) {
                    progressCallback(percentProgress);
                }
            } else {
                console.log('some error occurred');
            }
        };

        xhr.open(httpMethod, "http://hamletqa.zip.pr/hamlet/main/image");
        xhr.setRequestHeader("x-hamlet-api-key", this.API_KEY);
        xhr.setRequestHeader("event-id", 'hamletsp_dashboard_' + new Date().getTime());
        xhr.setRequestHeader("event-ts", new Date().getTime());
        xhr.setRequestHeader("cache-control", "no-cache");

        xhr.send(data);
    }
}