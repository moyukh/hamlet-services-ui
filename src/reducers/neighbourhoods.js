import * as Action from '../actions/ActionConstants.js';
import update from 'react-addons-update';

let initNeighbourhoodState = {
    subscribed: [],
    all: []
};

const neighbourhoods = (state = initNeighbourhoodState, action) => {
    switch (action.type) {
        case Action.LOGIN_SUCCESS:
            return update(state, {subscribed: {$set: action.subscribed_neighbourhoods}})
    }
    return state;
}

export default neighbourhoods;