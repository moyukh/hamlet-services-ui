import * as Action from '../../actions/ActionConstants.js';
import update from 'react-addons-update';

let initCampaignState = {
    search_text: "",
    selected_neighbourhoods: [],
    selected_neighbourhoods_map: {},
    post: {
        image: {
            file: {}
        },
        text: ""
    }
};

const campaign = (state = initCampaignState, action) => {
    switch (action.type) {
        case Action.NEIGHBOURHOOD_NAME_TXT_CHANGE:
            return update(state, {search_text: {$set: action.neighbourhood_name}});
        case Action.CAMPAIGN_NEIGHBOURHOOD_NAME_CLICK:
            return update(state, {
                selected_neighbourhoods: {
                    $unshift: [{
                        _id: action.neighbourhood_id,
                        name: action.neighbourhood_name
                    }]
                }, selected_neighbourhoods_map: {[action.neighbourhood_id]: {$set: action.neighbourhood_name}}
            });
        case Action.CAMPAIGN_NEIGHBOURHOOD_NAME_REMOVE:
            let selectedNeighbourhoods = [];
            for (let i = 0; i < state.selected_neighbourhoods.length; i++) {
                if (state.selected_neighbourhoods[i]._id !== action.neighbourhood_id) {
                    selectedNeighbourhoods.push(state.selected_neighbourhoods[i]);
                }
            }

            let selectedNeighbourhoodsMap = state.selected_neighbourhoods_map;
            selectedNeighbourhoodsMap = {...selectedNeighbourhoodsMap};
            delete selectedNeighbourhoodsMap[action.neighbourhood_id];
            return update(state, {
                selected_neighbourhoods_map: {$set: selectedNeighbourhoodsMap},
                selected_neighbourhoods: {$set: selectedNeighbourhoods}
            });
        case Action.POST_IMG_UPLOAD_PENDING:
            return update(state, {post: {image: {file: {$set: action.file}}}});
        case Action.POST_IMG_UPLOAD_PROGRESS:
            return update(state, {post: {image: {upload_percent: {$set: action.progress_percent}}}});
        case Action.POST_IMG_UPLOAD_SUCCESS:
            return update(state, {post: {image: {url: {$set: action.image_url}}}});
        case Action.POST_TXT_CHANGE:
            return update(state, {post: {text: {$set: action.post_txt}}});
        case Action.POST_SEND_CLICK:
            return update(state, {post: {status: {$set: 'pending'}}});
        case Action.POST_SEND_SUCCESS:
            return update(state, {post: {status: {$set: 'success'}}});
        case Action.POST_SEND_FAILURE:
            return update(state, {post: {status: {$set: 'error'}}});
        case Action.CREATE_NEW_POST
    }
    return state;
}

export default campaign;