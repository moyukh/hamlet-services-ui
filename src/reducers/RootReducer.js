import {combineReducers} from 'redux';
import * as Action from '../actions/ActionConstants.js';
import InitialState from '../../staticdata/InitialState.json';
import HamletServicesReducer from './HamletServicesReducer.js';

//Root Reducer of app
//On Logout sets state to InitialState
//Else delegates to HamletServicesReducer
const RootReducer = (state, action) => {
    switch (action.type) {
        case Action.LOGOUT:
            return InitialState
    }
    return HamletServicesReducer(state, action);
}

export default RootReducer;