import {combineReducers} from 'redux';
import * as Action from '../actions/ActionConstants.js';
import login from './login/login.js';
import session from './login/session.js';
import registration from './registration/registration.js';
import resetpassword from './resetpassword/resetpassword.js';
import campaign from './campaign/campaign.js';
import reports from './reports/reports.js';
import myprofile from './myprofile/myprofile.js'
import neighbourhoods from './neighbourhoods.js';

const HamletServicesReducer = combineReducers({
    login,
    session,
    registration,
    resetpassword,
    campaign,
    reports,
    myprofile,
    neighbourhoods
});

export default HamletServicesReducer;