import * as Action from '../../actions/ActionConstants.js';

const session = (state = {}, action) => {
    switch (action.type) {
        case Action.LOGIN_SUCCESS:
            return {
                username: action.username,
                sessiontoken: action['x-hamlet-sessiontoken'],
                display_name: action.display_name
            }
    }
    return state;
}

export default session;