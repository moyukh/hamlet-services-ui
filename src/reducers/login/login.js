import * as Action from '../../actions/ActionConstants.js';
import update from 'react-addons-update';

const login = (state = {}, action) => {
    switch (action.type) {
        case Action.GENERATE_LOGIN_OTP_CLICKED:
            return update(state, {otp_generation_status: {$set: 'clicked'}});
        case Action.GENERATE_LOGIN_OTP_SUCCESS:
            return update(state, {otp_generation_status: {$set: 'success'}});
        case Action.GENERATE_LOGIN_OTP_ERROR:
            return update(state, {otp_generation_status: {$set: 'error'}, otp_error: {$set: action.err_message}});
        case Action.MOBILE_NO_TXT_CHANGE:
            return {mobile_no: action.mobile_no};
        case Action.LOGIN_BUTTON_CLICK:
            return update(state, {login_state: {$set: 'clicked'}});
        case Action.LOGIN_FAILURE:
            return update(state, {login_state: {$set: 'error'}, login_error: {$set: action.err_message}});
        case Action.OTP_TXT_CHANGE:
            return update(state, {otp: {$set: action.otp}});
        case Action.LOGIN_SUCCESS:
            return {login_state: 'success'};
    }
    return state;
}

export default login;